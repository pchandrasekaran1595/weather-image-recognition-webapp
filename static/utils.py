import io
import cv2
import onnx
import json
import base64
import numpy as np
import onnxruntime as ort

from PIL import Image


MEAN, STD = [0.47442, 0.47353, 0.47343], [0.27100, 0.26926, 0.26821]


class ImageCodecs(object):

    def __init__(self):
        pass

    def decode_image(self, imageData) -> np.ndarray:
        header, imageData = imageData.split(",")[0], imageData.split(",")[1]
        image = np.array(Image.open(io.BytesIO(base64.b64decode(imageData))))
        image = cv2.cvtColor(src=image, code=cv2.COLOR_BGRA2RGB)
        return header, image


    def encode_image(self, header: str, image: np.ndarray) -> str:
        _, imageData = cv2.imencode(".jpeg", image)
        imageData = base64.b64encode(imageData)
        imageData = str(imageData).replace("b'", "").replace("'", "")
        imageData = header + "," + imageData
        return imageData

image_codecs = ImageCodecs()    


class Inferer(object):
    def __init__(self):
        path = "static/model.onnx"
        model = onnx.load(path)
        onnx.checker.check_model(model)
        self.ort_session = ort.InferenceSession(path)
        self.size = 320
        self.labels = json.load(open("static/labels.json", "r"))
    
    def infer(self, image: np.ndarray) -> str:
        image = cv2.resize(src=cv2.cvtColor(src=image, code=cv2.COLOR_BGRA2RGB), dsize=(self.size, self.size), interpolation=cv2.INTER_AREA)
        image = image / 255
        for i in range(image.shape[2]):
            image[:, :, i] = (image[:, :, i] - MEAN[i]) / STD[i]
        image = image.transpose(2, 0, 1)
        image = np.expand_dims(image, axis=0).astype("float32")

        output = self.ort_session.run(None, {"input" : image.astype("float32")})

        return self.labels[str(np.argmax(output[0]))].title()

inferer = Inferer()
