import json


import json

from django.shortcuts import render
from django.http import JsonResponse

from static.utils import image_codecs, inferer

def index(request):
    if request.method == "POST":
        JSONData = request.POST.get("data")

        imageData = json.loads(JSONData)["imageData"]
        _, image = image_codecs.decode_image(imageData)
        label = inferer.infer(image)

        return JsonResponse({
            "label" : label
        })

    return render(request=request, template_name="index.html", context={})
